import {Injectable} from "@angular/core";
import {Http} from "@angular/http";

import 'rxjs/Rx';
import {Observable} from 'rxjs/Observable';

import {User} from "../../core/user";
import {API_URL} from "../../core/core.module";

@Injectable()
export class AuthService {

    isLoggedIn: boolean = false;

    constructor(private http: Http) {
    }

    /**
     *
     * @param user
     * @returns {Promise<TResult|T>|Promise<T>}
     */
    login(user: User): Observable<boolean> {
        return this.http.post(API_URL + "/login", user)
            .map(response => response.json() as User)
            .map(user => {
                if (!User.isNull(user)) {
                    this.isLoggedIn = true;
                    return true
                } else {
                    this.isLoggedIn = false;
                    return false
                }
            })
            .catch(AuthService.handleError);
    }

    logOut(): Observable<boolean> {
        this.isLoggedIn = !this.isLoggedIn;
        return Observable.of(false);
    }

    /**
     * Handle register request.
     * @param user
     * @returns {Promise<TResult|T>|Promise<T>}
     */
    register(user: User): Observable<boolean> {
        return this.http.post(API_URL + "/register", user)
            .map(response => response.json() as User)
            .map(user => !User.isNull(user))
            .catch(AuthService.handleError);
    }

    /**
     * Handle Error events.
     * @param error
     * @returns {any}
     */
    private static handleError(error: any) {
        let errorMessage = (error.message) ? error.message :
            error.status ? `${error.status} - ${error.statusText}` : `Server error`;
        console.log(errorMessage);
        return Observable.throw(errorMessage);
    }
}