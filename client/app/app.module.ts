import {NgModule} from "@angular/core";
import {BrowserModule} from "@angular/platform-browser";

import {CoreModule} from "./core/core.module";
import {AuthModule} from "./auth/auth.module";
import {HomeModule} from "./home/home.module";
import {routing} from "./app.routing";

import {AppComponent} from "./app.component";
import {CommonModule} from "@angular/common";
import { FormsModule }   from '@angular/forms';

@NgModule({
    imports: [
        BrowserModule,
        CoreModule,
        AuthModule,
        HomeModule,
        CommonModule,
        routing,
        FormsModule
    ],
    declarations: [
        AppComponent,
    ],
    bootstrap: [AppComponent]
})
export class AppModule {}