import {Injectable} from "@angular/core";
import {User} from "./user";
import {Http} from "@angular/http";

import {Observable} from "rxjs";
import {API_URL} from "./core.module";

import "rxjs/add/operator/map";
import "rxjs/add/operator/catch";

@Injectable()
export class UserService {

    constructor(private http : Http) {}

    getUsers(): Observable<User[]> {
        return this.http.get(API_URL + '/users')
            .map(response => response.json());
    }

    getUser(userId: string): Observable<User> {
        return this.http.get(API_URL + '/user/' + userId)
            .map(response => response.json());
    }

    private extractData(res:Response) {
        let body = res.json();
        return body || [];
    }
}