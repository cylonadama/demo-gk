import {Component, NgModule, OnInit} from "@angular/core";
import {AuthService} from "../auth/services/auth.service";
import {Router} from "@angular/router";
import {UserService} from "../core/user.service";
import {User} from "../core/user";
import { CommonModule } from '@angular/common';
// import {FormsModule} from "@angular/forms";
import {Observable} from "rxjs/Observable";

@NgModule({
    imports: [
        CommonModule
    ]
})
@Component({
    moduleId: module.id,
    providers:[
        UserService
    ],
    selector: 'home',
    templateUrl: './home.component.html'
})

export class HomeComponent implements OnInit{
    //Properties
    public allUsers: User[] = [];
    public statusCode: number;
    public usr;
    public foo = ['a', 'b', 'c'];

    constructor(private router: Router,
                private authService: AuthService, private userService: UserService) {
    }

    ngOnInit() {
        this.userService.getUsers().subscribe( stuff => {
            this.allUsers = stuff;
        });

    }

    logOut(): void {
        this.authService.logOut().subscribe(isLoggedIn => {
            if( isLoggedIn === false) {
                this.router.navigate(['/auth']);
            }
        })
    }

}