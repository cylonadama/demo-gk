package com.gk.service;

import com.gk.model.User;
import com.gk.repository.UserRepository;
import org.apache.commons.collections4.IteratorUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;


    @Autowired
    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @PostConstruct
    private void populateSampleData() {
        userRepository.save(new User("email@test", "pass", "firstname", "lastname","token"));
    }

    /**
     * return all users.
     * @return
     */
    @Override
    public List<User> getUsers() {
        Iterator<User> userIterator = userRepository.findAll().iterator();
        return IteratorUtils.toList(userIterator);
    }

    /**
     * Get a user based on the id
     * @param id
     * @return
     */
    @Override
    public User getUser(Long id) {
        return userRepository.findOne(id);
    }

    /**
     * Create a user and set the token.
     * @param user
     * @return
     */
    @Override
    public User createUser(User user) {
        // Creating a random UUID (Universally unique identifier).
        UUID uuid = UUID.randomUUID();
        String randomUUIDString = uuid.toString();
        user.setToken(randomUUIDString);
        return userRepository.save(user);
    }

    @Override
    public User findUserByEmail(String email) {
        return userRepository.findOneByEmail(email);
    }

    @Override
    public User findUserByEmailAndPassword(String email, String password) {
        return userRepository.findOneByEmailAndPassword(email, password);
    }
}
