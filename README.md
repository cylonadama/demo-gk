This App has an angular 2 front end with the server 
using the Spring Framework.
I am using gradle for the build process.

To Run App
* Insure you have the following dependencies installed:
 Gradle

* Run  `gradle buildRoot`

* cd into the client directory 
and run `npm start`. This will start up the client.

Issues outstanding:
 *angular template issue blocking progress.